# Carpoolingv3
Este proyecto fue realizado bajo los requerimientos del reto 3 del BootCamp PowerUp Java x AWS y su funcionalidad es crear un registro en BD MySQL de una ruta + usuario(conductor) + viajes + barrios por medio de un servicio REST y teniendo en cuenta las validaciones requeridas.


## End Point Para Registrar Usuario
Post: localhost:8080/usuario/signup

## JSON de la peticion:
```json

{
  "name":"Ana",
  "email": "ana.castro@email.com",
  "familyName": "Castro",
  "address": "Calle falsa #1234",
  "phoneNumber": "+573007698329",
  "password": "HolaMundo1234*"
}

```

## End Point Para Iniciar Sesion Como Usuario
localhost:8080/usuario/login

## JSON de la peticion:
```json

{
  "email":"ana.castro@email.com",
  "password": "HolaMundo1234*"
}

```


## End Point Para Crear Ruta Con Todos Los Datos Requeridos
Este endpoint debe llevar el token de seguridad
Post: localhost:8080/ruta/guardarRutaCompleta

## Token
Authorization
Bearer Token: <valor idToken>

## JSON de la peticion:
```json

{
    "descripcion":"Norte-Sur",
    "cupos":4,
    "usuario":{
        "email":"ana.castro@email.com"
    },
    "barriosList":[
    
        {
            "nombre":"Santa Ana",
            "descripcion":"Norte"
        },
        {
            "nombre":"San Jose",
            "descripcion":"Sur"
        }
    ],
    "viajesList":[
        {
            "horario":"2017-01-18T17:09:42.411"
        },
        {
            "horario":"2017-01-13T17:09:42.411"
        }
    ]
}

```
## NOTA:
Antes de compilar el proyecto recuerde configurar el archivo application.properties según sus configuraciones locales

package com.pragma.carpooling.application.handler;

import com.pragma.carpooling.domain.model.Barrio;

import java.util.List;

public interface IBarrioHandler {

    List<Barrio> listarBarios();
}

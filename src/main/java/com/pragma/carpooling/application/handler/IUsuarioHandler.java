package com.pragma.carpooling.application.handler;

import com.pragma.carpooling.application.dto.request.UsuarioLogInRequestDto;
import com.pragma.carpooling.application.dto.request.UsuarioRequest;
import com.pragma.carpooling.application.dto.response.MensajeResponseDto;
import com.pragma.carpooling.application.dto.response.UsuarioLoginResponseDto;
import com.pragma.carpooling.domain.model.MensajeRespuesta;

public interface IUsuarioHandler {

    MensajeResponseDto registrarUsuario(UsuarioRequest usuarioRequest);

    UsuarioLoginResponseDto iniciarSesion(UsuarioLogInRequestDto usuarioLogInRequestDto);
}

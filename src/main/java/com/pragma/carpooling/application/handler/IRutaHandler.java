package com.pragma.carpooling.application.handler;

import com.pragma.carpooling.application.dto.request.RutaCompletaRequest;
import com.pragma.carpooling.application.dto.response.MensajeResponseDto;

public interface IRutaHandler {

    MensajeResponseDto guardarRutaCompleta(RutaCompletaRequest rutaCompletaRequest);

}

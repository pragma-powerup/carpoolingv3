package com.pragma.carpooling.application.handler.impl;

import com.pragma.carpooling.application.dto.request.UsuarioLogInRequestDto;
import com.pragma.carpooling.application.dto.request.UsuarioRequest;
import com.pragma.carpooling.application.dto.response.MensajeResponseDto;
import com.pragma.carpooling.application.dto.response.UsuarioLoginResponseDto;
import com.pragma.carpooling.application.handler.IUsuarioHandler;
import com.pragma.carpooling.application.mapper.IMensajeResponseMapper;
import com.pragma.carpooling.application.mapper.IUsuarioLoginRequestMapper;
import com.pragma.carpooling.application.mapper.IUsuarioRequestMapper;
import com.pragma.carpooling.application.mapper.IUsuarioResponseMapper;
import com.pragma.carpooling.domain.api.IUsuarioServicePort;
import com.pragma.carpooling.domain.model.CognitoToken;
import com.pragma.carpooling.domain.model.MensajeRespuesta;
import com.pragma.carpooling.domain.model.Usuario;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Transactional
public class UsuarioHandler implements IUsuarioHandler {

    private final IUsuarioServicePort usuarioServicePort;
    private final IUsuarioRequestMapper usuarioRequestMapper;
    private final IUsuarioLoginRequestMapper usuarioLoginRequestMapper;
    private final IUsuarioResponseMapper usuarioResponseMapper;
    private final IMensajeResponseMapper mensajeResponseMapper;

    @Override
    public MensajeResponseDto registrarUsuario(UsuarioRequest usuarioRequest) {
        Usuario usuario = usuarioRequestMapper.toUsuario(usuarioRequest);
        return mensajeResponseMapper.toResponse(usuarioServicePort.guardarUsuario(usuario));
    }

    @Override
    public UsuarioLoginResponseDto iniciarSesion(UsuarioLogInRequestDto usuarioLoginRequestDto) {
        Usuario usuario = usuarioLoginRequestMapper.toUsuario(usuarioLoginRequestDto);
        CognitoToken cognitoToken = usuarioServicePort.iniciarSesion(usuario);
        return usuarioResponseMapper.toResponse(cognitoToken);
    }



}

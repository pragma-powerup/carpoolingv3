package com.pragma.carpooling.application.handler.impl;

import com.pragma.carpooling.application.handler.IBarrioHandler;
import com.pragma.carpooling.domain.api.IBarrioServicePort;
import com.pragma.carpooling.domain.model.Barrio;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class BarrioHandler implements IBarrioHandler {

    private final IBarrioServicePort barrioServicePort;

    @Override
    public List<Barrio> listarBarios() {
        return barrioServicePort.listarBarrios();
    }
}

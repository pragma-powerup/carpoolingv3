package com.pragma.carpooling.application.handler.impl;

import com.pragma.carpooling.application.dto.request.RutaCompletaRequest;
import com.pragma.carpooling.application.dto.response.MensajeResponseDto;
import com.pragma.carpooling.application.handler.IRutaHandler;
import com.pragma.carpooling.application.mapper.IRutaCompletaRequestMapper;
import com.pragma.carpooling.domain.api.IBarrioServicePort;
import com.pragma.carpooling.domain.api.IRutaBarrioServicePort;
import com.pragma.carpooling.domain.api.IRutaServicePort;
import com.pragma.carpooling.domain.api.IViajeServicePort;
import com.pragma.carpooling.domain.model.Barrio;
import com.pragma.carpooling.domain.model.Ruta;
import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.model.Viaje;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@RequiredArgsConstructor
@Transactional
public class RutaHandler implements IRutaHandler {

    private final IRutaServicePort rutaServicePort;
    private final IBarrioServicePort barrioServicePort;
    private final IRutaBarrioServicePort rutaBarrioServicePort;
    private final IViajeServicePort viajeServicePort;
    private final IRutaCompletaRequestMapper IRutaCompletaRequestMapper;

    @Override
    public MensajeResponseDto guardarRutaCompleta(RutaCompletaRequest rutaCompletaRequest) {
        Usuario usuario = IRutaCompletaRequestMapper.toUsuario(rutaCompletaRequest.getUsuario());
        Ruta ruta = IRutaCompletaRequestMapper.toRuta(rutaCompletaRequest);
        List<Barrio> barriosList = IRutaCompletaRequestMapper.toBarrioList(rutaCompletaRequest.getBarriosList());
        List<Viaje> viajesList = IRutaCompletaRequestMapper.toViajeList(rutaCompletaRequest.getViajesList());
        Ruta rutaGuardada = rutaServicePort.guardarRuta(ruta, usuario, barriosList, viajesList);
        return new MensajeResponseDto("Se ha creado la ruta completa con el id: "+ rutaGuardada.getIdRuta());
    }


}

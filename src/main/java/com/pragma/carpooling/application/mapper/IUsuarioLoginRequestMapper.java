package com.pragma.carpooling.application.mapper;

import com.pragma.carpooling.application.dto.request.UsuarioLogInRequestDto;
import com.pragma.carpooling.domain.model.Usuario;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface IUsuarioLoginRequestMapper {

    Usuario toUsuario (UsuarioLogInRequestDto usuarioLogInRequestDto);
}

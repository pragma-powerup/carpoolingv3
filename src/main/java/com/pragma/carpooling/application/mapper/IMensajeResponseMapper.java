package com.pragma.carpooling.application.mapper;

import com.pragma.carpooling.application.dto.response.MensajeResponseDto;
import com.pragma.carpooling.domain.model.MensajeRespuesta;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface IMensajeResponseMapper {

    MensajeResponseDto toResponse(MensajeRespuesta mensaje);
}

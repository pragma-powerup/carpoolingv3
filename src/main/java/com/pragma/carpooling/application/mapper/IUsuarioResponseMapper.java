package com.pragma.carpooling.application.mapper;

import com.pragma.carpooling.application.dto.response.UsuarioLoginResponseDto;
import com.pragma.carpooling.domain.model.CognitoToken;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface IUsuarioResponseMapper {

    UsuarioLoginResponseDto toResponse(CognitoToken cognitoToken);

}

package com.pragma.carpooling.application.mapper;

import com.pragma.carpooling.application.dto.request.BarrioRequest;
import com.pragma.carpooling.application.dto.request.RutaCompletaRequest;
import com.pragma.carpooling.application.dto.request.UsuarioRequest;
import com.pragma.carpooling.application.dto.request.ViajeRequest;
import com.pragma.carpooling.domain.model.Barrio;
import com.pragma.carpooling.domain.model.Ruta;
import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.model.Viaje;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface IRutaCompletaRequestMapper {

    Ruta toRuta(RutaCompletaRequest rutaCompletaRequest);

    Usuario toUsuario(UsuarioRequest usuarioRequest);

    List<Barrio> toBarrioList(List<BarrioRequest> barrioRequestList);

    List<Viaje> toViajeList(List<ViajeRequest> viajeRequestList);

}

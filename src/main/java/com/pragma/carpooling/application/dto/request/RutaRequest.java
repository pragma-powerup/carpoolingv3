package com.pragma.carpooling.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RutaRequest {
    private String descripcion;
    private Integer cupos;
}

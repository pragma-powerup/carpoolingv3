package com.pragma.carpooling.application.dto.request;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class ViajeRequest {
    private LocalDateTime horario;
}

package com.pragma.carpooling.application.dto.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UsuarioLoginResponseDto {
    private String idToken;
}

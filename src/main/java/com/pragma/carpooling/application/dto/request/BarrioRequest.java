package com.pragma.carpooling.application.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BarrioRequest {
    private String nombre;
    private String descripcion;
}

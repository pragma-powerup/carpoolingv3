package com.pragma.carpooling.domain.usecase;

import com.pragma.carpooling.domain.api.IUsuarioServicePort;
import com.pragma.carpooling.domain.model.CognitoToken;
import com.pragma.carpooling.domain.model.MensajeRespuesta;
import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.spi.ICognitoPersistencePort;
import com.pragma.carpooling.domain.spi.IUsuarioPersistencePort;
import com.pragma.carpooling.domain.validation.ValidacionUsuario;


public class UsuarioUseCase implements IUsuarioServicePort {

    private final IUsuarioPersistencePort usuarioPersistencePort;
    private final ICognitoPersistencePort cognitoPersistencePort;

    public UsuarioUseCase(IUsuarioPersistencePort usuarioPersistencePort, ICognitoPersistencePort cognitoPersistencePort) {
        this.usuarioPersistencePort = usuarioPersistencePort;
        this.cognitoPersistencePort = cognitoPersistencePort;
    }

    @Override
    public MensajeRespuesta guardarUsuario(Usuario usuario) {

        ValidacionUsuario.validarObjetoUsuario(usuario);
        usuarioPersistencePort.validarEmailUnico(usuario.getEmail());

        String username = cognitoPersistencePort.signupUser(usuario);
        usuario.setUsername(username);

        Usuario usuarioCreado = usuarioPersistencePort.guardarUsuario(usuario);
        return new MensajeRespuesta(String.format("Usuario creado con id %d", usuarioCreado.getIdUsuario()));

    }

    @Override
    public CognitoToken iniciarSesion(Usuario usuario) {
        ValidacionUsuario.validarLoginUsuario(usuario);

        Usuario usuarioAutenticado = usuarioPersistencePort.obtenerUsuarioPorPasswordeEmail(usuario);
        return cognitoPersistencePort.loginUser(usuarioAutenticado);
    }


    @Override
    public Usuario buscarUsuarioPorEmail(String email) {
        return usuarioPersistencePort.obtenerUsuarioPorEmail(email);
    }


}


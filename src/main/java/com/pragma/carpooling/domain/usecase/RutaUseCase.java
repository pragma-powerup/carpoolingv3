package com.pragma.carpooling.domain.usecase;

import com.pragma.carpooling.domain.api.IBarrioServicePort;
import com.pragma.carpooling.domain.api.IRutaBarrioServicePort;
import com.pragma.carpooling.domain.api.IRutaServicePort;
import com.pragma.carpooling.domain.api.IUsuarioServicePort;
import com.pragma.carpooling.domain.api.IViajeServicePort;
import com.pragma.carpooling.domain.model.Barrio;
import com.pragma.carpooling.domain.model.Ruta;
import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.model.Viaje;
import com.pragma.carpooling.domain.spi.IBarrioPersistencePort;
import com.pragma.carpooling.domain.spi.IRutaPersistencePort;

import com.pragma.carpooling.domain.spi.IUsuarioPersistencePort;
import com.pragma.carpooling.infrastructure.exception.CuposInvalidosException;
import lombok.RequiredArgsConstructor;

import java.util.List;


@RequiredArgsConstructor
public class RutaUseCase implements IRutaServicePort {

    private final IRutaPersistencePort rutaPersistencePort;
    private final IUsuarioServicePort usuarioServicePort;
    private final IBarrioServicePort barrioServicePort;
    private final IViajeServicePort viajeServicePort;
    private final IRutaBarrioServicePort rutaBarrioServicePort;
    @Override
    public Ruta guardarRuta(Ruta ruta, Usuario usuario, List<Barrio> barriosList, List<Viaje> viajesList) {

        if(!validacionCantidadDeCupos(ruta.getCupos())){
            throw new CuposInvalidosException();
        }
        Usuario usuarioEncontrado = usuarioServicePort.buscarUsuarioPorEmail(usuario.getEmail());
        ruta.setIdUsuario(usuarioEncontrado.getIdUsuario());
        Ruta rutaPersistida = rutaPersistencePort.guardarRuta(ruta);
        List<Barrio> barriosPersistidos = barrioServicePort.guardarListabarrios(barriosList);
        rutaBarrioServicePort.guardarRutaBarrio(barriosPersistidos, rutaPersistida.getIdRuta());
        viajeServicePort.guardarListaViajes(viajesList, rutaPersistida.getIdRuta());
        return rutaPersistida;
    }

    private boolean validacionCantidadDeCupos(Integer cupos ){
        if(cupos > 4 || cupos < 1){
            return false;
        }
        return true;
    }

}

package com.pragma.carpooling.domain.spi;

import com.pragma.carpooling.domain.model.CognitoToken;
import com.pragma.carpooling.domain.model.Usuario;

public interface ICognitoPersistencePort {

    String signupUser(Usuario usuario);

    CognitoToken loginUser(Usuario usuario);

}

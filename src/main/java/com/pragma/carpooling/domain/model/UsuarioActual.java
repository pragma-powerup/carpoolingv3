package com.pragma.carpooling.domain.model;

public class UsuarioActual {

    private String username;

    public UsuarioActual() {
    }

    public UsuarioActual(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

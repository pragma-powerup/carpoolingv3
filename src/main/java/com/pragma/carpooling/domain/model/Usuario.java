package com.pragma.carpooling.domain.model;

public class Usuario {

    private Long idUsuario;
    private String name;
    private String familyName;
    private String phoneNumber;
    private String password;
    private String email;
    private String username;
    private String address;

    public Usuario() {
    }

    public Usuario(Long idUsuario, String name, String familyName, String phoneNumber, String password, String email, String username) {
        this.idUsuario = idUsuario;
        this.name = name;
        this.familyName = familyName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.email = email;
        this.username = username;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

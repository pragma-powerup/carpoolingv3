package com.pragma.carpooling.domain.api;

import com.pragma.carpooling.domain.model.CognitoToken;
import com.pragma.carpooling.domain.model.MensajeRespuesta;
import com.pragma.carpooling.domain.model.Usuario;


public interface IUsuarioServicePort {

    MensajeRespuesta guardarUsuario(Usuario usuario);

    CognitoToken iniciarSesion(Usuario usuario);

    Usuario buscarUsuarioPorEmail(String email);


}

package com.pragma.carpooling.domain.validation;

import com.pragma.carpooling.infrastructure.exception.CamposInvalidosException;
import com.pragma.carpooling.infrastructure.exception.InvalidEmailException;
import com.pragma.carpooling.infrastructure.exception.InvalidPasswordException;
import org.apache.logging.log4j.util.Strings;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils {

    private ValidationUtils(){

    }

    private static String PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[*_-]).{8,15}$";

    private static final Pattern passwordPattern = Pattern.compile(PASSWORD_PATTERN);

    private static boolean isValidPassword(String s){
        if(Strings.isBlank(s)){
            return false;
        }
        Matcher matcher = passwordPattern.matcher(s);
        return matcher.matches();
    }

    private static boolean isValidEmail(String email){
        EmailValidator validator = EmailValidator.getInstance();
        return validator.isValid(email);
    }

    private static boolean isBlank(final CharSequence cs) {
        final int strLen = length(cs);
        if (strLen == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if (!Character.isWhitespace(cs.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    private static int length(final CharSequence cs) {
        return cs == null ? 0 : cs.length();
    }

    public static <T> T requireNonNull(T obj, String field) {
        if (obj == null) {
            throw new CamposInvalidosException(field + " no debería ser null");
        }
        return obj;
    }

    public static int requireBetween(int value, int min, int max, String field) {
        if (!(value >= min && value <= max)) {
            throw new CamposInvalidosException(field + " debería estar entre "+ min +" y "+ max);
        }
        return value;
    }

    public static int requireGreaterOrEqualThan(int value, int min, String field) {
        if (value < min) {
            throw new CamposInvalidosException(field + " debería ser mayor o igual que " + min);
        }
        return value;
    }

    public static void requireNonBlank(String s, String field) {
        if (isBlank(s)) {
            throw new CamposInvalidosException(field + " no deberia ser vacío");
        }
    }

    public static void requireValidEmail(String s) {
        if (!isValidEmail(s)) {
            throw new InvalidEmailException();
        }
    }

    public static void requireValidPassword(String s) {
        if (!isValidPassword(s)) {
            throw new InvalidPasswordException();
        }
    }


}

package com.pragma.carpooling.domain.validation;

import com.pragma.carpooling.domain.model.Usuario;

import static com.pragma.carpooling.domain.validation.ValidationUtils.requireNonBlank;
import static com.pragma.carpooling.domain.validation.ValidationUtils.requireNonNull;
import static com.pragma.carpooling.domain.validation.ValidationUtils.requireValidEmail;
import static com.pragma.carpooling.domain.validation.ValidationUtils.requireValidPassword;


public class ValidacionUsuario {

    private ValidacionUsuario(){

    }

    public static void validarObjetoUsuario(final Usuario usuario){
        requireNonNull(usuario, "User");
        requireNonBlank(usuario.getName(), "name");
        requireNonBlank(usuario.getFamilyName(), "familyName");
        requireValidEmail(usuario.getEmail());
        requireNonBlank(usuario.getPhoneNumber(), "phoneNumber");
        requireValidPassword(usuario.getPassword());
    }
    public static void validarLoginUsuario(Usuario usuario) {
        requireNonNull(usuario, "User");
        requireValidEmail(usuario.getEmail());
        requireNonBlank(usuario.getPassword(), "password");
    }

}

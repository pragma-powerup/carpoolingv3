package com.pragma.carpooling.infrastructure.out.http.mapper;

import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoLoginUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface ICognitoLoginModelMapper {

    @Mapping(target = "name", source = "username")
    CognitoLoginUserRequest toModel(Usuario usuario);
}

package com.pragma.carpooling.infrastructure.out.http.service;

import com.pragma.carpooling.infrastructure.out.http.model.CognitoLoginUserRequest;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoSignUpUserRequest;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoTokenResponse;

public interface ICognitoService {

    String signup(CognitoSignUpUserRequest signUpUserRequest);

    CognitoTokenResponse login(CognitoLoginUserRequest cognitoLoginUserRequest);
}

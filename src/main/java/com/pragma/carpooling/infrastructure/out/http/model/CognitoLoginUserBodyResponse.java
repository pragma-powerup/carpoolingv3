package com.pragma.carpooling.infrastructure.out.http.model;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CognitoLoginUserBodyResponse {

    @SerializedName("Errors")
    private List<String> errors;

    @SerializedName("Data")
    private CognitoTokenResponse tokenResponse;

    @SerializedName("Message")
    private String message;
}

package com.pragma.carpooling.infrastructure.out.http.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CognitoSignUpUserRequest {
    private String name;
    private String email;
    private String familyName;
    private String address;
    private String phoneNumber;
    private String password;
}

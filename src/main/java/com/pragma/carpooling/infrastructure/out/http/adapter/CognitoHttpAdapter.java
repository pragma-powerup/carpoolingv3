package com.pragma.carpooling.infrastructure.out.http.adapter;

import com.pragma.carpooling.domain.model.CognitoToken;
import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.spi.ICognitoPersistencePort;
import com.pragma.carpooling.infrastructure.out.http.mapper.ICognitoLoginModelMapper;
import com.pragma.carpooling.infrastructure.out.http.mapper.ICognitoSignUpModelMapper;
import com.pragma.carpooling.infrastructure.out.http.mapper.ICognitoTokenModelMapper;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoTokenResponse;
import com.pragma.carpooling.infrastructure.out.http.service.ICognitoService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CognitoHttpAdapter implements ICognitoPersistencePort {

    private final ICognitoService cognitoService;
    private final ICognitoSignUpModelMapper cognitoSignUpModelMapper;
    private final ICognitoLoginModelMapper cognitoLoginModelMapper;
    private final ICognitoTokenModelMapper cognitoTokenModelMapper;

    @Override
    public String signupUser(Usuario usuario) {
        return cognitoService.signup(cognitoSignUpModelMapper.toModel(usuario));
    }

    @Override
    public CognitoToken loginUser(Usuario usuario) {
        CognitoTokenResponse cognitoTokenResponse = cognitoService.login(cognitoLoginModelMapper.toModel(usuario));
        return cognitoTokenModelMapper.toModel(cognitoTokenResponse);
    }
}

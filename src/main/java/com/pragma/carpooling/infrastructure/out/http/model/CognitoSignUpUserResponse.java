package com.pragma.carpooling.infrastructure.out.http.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CognitoSignUpUserResponse {
    private Integer statusCode;
    private String body;
    private Boolean isBase64Encoded;
}

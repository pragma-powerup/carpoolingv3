package com.pragma.carpooling.infrastructure.out.jpa.adapter;

import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.spi.IUsuarioPersistencePort;
import com.pragma.carpooling.infrastructure.exception.CamposInvalidosException;
import com.pragma.carpooling.infrastructure.exception.UserAlreadyExistsException;
import com.pragma.carpooling.infrastructure.exception.UserNotFoundException;
import com.pragma.carpooling.infrastructure.out.jpa.entity.UsuarioEntity;
import com.pragma.carpooling.infrastructure.out.jpa.mapper.UsuarioEntityMapper;
import com.pragma.carpooling.infrastructure.out.jpa.repository.IUsuarioRepository;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class UsuarioJpaAdapter implements IUsuarioPersistencePort {

    private final IUsuarioRepository usuarioRepository;
    private final UsuarioEntityMapper usuarioEntityMapper;

    @Override
    public Usuario guardarUsuario(Usuario usuario) {
        if(usuarioRepository.findByEmail(usuario.getEmail()).isPresent()){
            throw new UserAlreadyExistsException();
        }
        UsuarioEntity usuarioEntity = usuarioRepository.save(usuarioEntityMapper.toEntity(usuario));
        return usuarioEntityMapper.toUsuario(usuarioEntity);
    }

    @Override
    public Usuario obtenerUsuarioPorEmail(String emailUsuario) {
        return usuarioEntityMapper.toUsuario(usuarioRepository.findByEmail(emailUsuario)
                .orElseThrow(UserNotFoundException::new));
    }

    @Override
    public void validarEmailUnico(String email) {
        if(usuarioRepository.findByEmail(email).isPresent()){
            throw new UserAlreadyExistsException();
        }
    }

    @Override
    public Usuario obtenerUsuarioPorPasswordeEmail(Usuario usuario) {
        Optional<UsuarioEntity> usuarioEntityOptional = usuarioRepository.findByPasswordAndEmail(usuario.getPassword(), usuario.getEmail());
        if(usuarioEntityOptional.isEmpty()){
            throw new UserNotFoundException();
        }
        return usuarioEntityMapper.toUsuario(usuarioEntityOptional.get());
    }

}

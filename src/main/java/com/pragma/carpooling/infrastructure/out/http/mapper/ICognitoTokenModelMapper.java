package com.pragma.carpooling.infrastructure.out.http.mapper;

import com.pragma.carpooling.domain.model.CognitoToken;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoTokenResponse;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface ICognitoTokenModelMapper {

    CognitoToken toModel(CognitoTokenResponse cognitoTokenResponse);
}

package com.pragma.carpooling.infrastructure.out.http.mapper;

import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoSignUpUserRequest;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface ICognitoSignUpModelMapper {

    CognitoSignUpUserRequest toModel(Usuario usuario);
}

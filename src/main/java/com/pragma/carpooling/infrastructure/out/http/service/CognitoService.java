package com.pragma.carpooling.infrastructure.out.http.service;

import com.google.gson.Gson;
import com.pragma.carpooling.infrastructure.exception.CognitoException;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoLoginUserBodyResponse;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoLoginUserRequest;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoLoginUserResponse;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoSignUpUserRequest;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoSignUpUserResponse;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoSignUpUserBodyResponse;
import com.pragma.carpooling.infrastructure.out.http.model.CognitoTokenResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class CognitoService implements ICognitoService{

    private static final String BASE_URL = "https://k6gwndgg8j.execute-api.us-east-1.amazonaws.com/pdn/bootcamppowerup";

    private MultiValueMap<String, String> getHeaders() {
        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        Map<String, String> map = new HashMap<>();
        map.put("Content-Type", "application/json");
        headers.setAll(map);
        return headers;
    }


    @Override
    public String signup(CognitoSignUpUserRequest signUpUserRequest) {
        HttpEntity<CognitoSignUpUserRequest> request = new HttpEntity<>(signUpUserRequest, getHeaders());
        String url = BASE_URL + "/Signup";
        ResponseEntity<CognitoSignUpUserResponse> responseEntity = new RestTemplate()
                .postForEntity(url, request, CognitoSignUpUserResponse.class);

        CognitoSignUpUserResponse signUpUserResponse = responseEntity.getBody();
        if(signUpUserResponse == null){
            throw new CognitoException();
        }
        Gson gson = new Gson();
        CognitoSignUpUserBodyResponse userData = gson.fromJson(
                signUpUserResponse.getBody(),
                CognitoSignUpUserBodyResponse.class
        );

        if (StringUtils.isEmpty(userData.getData())){
            throw new CognitoException();
        }
        return userData.getData();
    }

    @Override
    public CognitoTokenResponse login(CognitoLoginUserRequest cognitoLoginUserRequest) {
        HttpEntity<CognitoLoginUserRequest> request = new HttpEntity<>(cognitoLoginUserRequest, getHeaders());
        String url = BASE_URL + "/Login";

        ResponseEntity<CognitoLoginUserResponse> responseEntity = new RestTemplate()
                .postForEntity(url, request, CognitoLoginUserResponse.class);
        
        CognitoLoginUserResponse loginUserResponse = responseEntity.getBody();

        if(loginUserResponse == null){
            throw new CognitoException();
        }

        Gson gson = new Gson();
        CognitoLoginUserBodyResponse cognitoLoginUserBodyResponse = gson.fromJson(
                loginUserResponse.getBody(),
                CognitoLoginUserBodyResponse.class
        );


        if(cognitoLoginUserBodyResponse.getTokenResponse() == null){
            throw new CognitoException();
        }

        return cognitoLoginUserBodyResponse.getTokenResponse();

    }
}

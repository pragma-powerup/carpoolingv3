package com.pragma.carpooling.infrastructure.out.jpa.adapter;

import com.pragma.carpooling.domain.model.Ruta;
import com.pragma.carpooling.domain.model.Viaje;
import com.pragma.carpooling.domain.spi.IViajePersistencePort;
import com.pragma.carpooling.infrastructure.exception.RutaNotFoundException;
import com.pragma.carpooling.infrastructure.out.jpa.entity.RutaEntity;
import com.pragma.carpooling.infrastructure.out.jpa.repository.IRutaRepository;
import com.pragma.carpooling.infrastructure.out.jpa.repository.IViajeRepository;
import lombok.RequiredArgsConstructor;


@RequiredArgsConstructor
public class ViajeJpaAdapter implements IViajePersistencePort {

    private final IViajeRepository viajeRepository;
    private final IRutaRepository rutaRepository;

    @Override
    public void guardarViajesConRuta(Long idRuta, Viaje viaje) {
         RutaEntity ruta = rutaRepository.findById(idRuta).orElseThrow(()-> new RutaNotFoundException());
         viajeRepository.registrarConRuta(ruta.getIdRuta(), viaje.getHorario());
    }

}

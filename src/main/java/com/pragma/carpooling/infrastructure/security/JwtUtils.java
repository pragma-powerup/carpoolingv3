package com.pragma.carpooling.infrastructure.security;

import javax.servlet.http.HttpServletRequest;

import com.google.gson.stream.MalformedJsonException;
import com.nimbusds.jwt.SignedJWT;
import com.google.gson.Gson;
import com.pragma.carpooling.infrastructure.exception.InvalidJwtTokenException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.text.ParseException;

public class JwtUtils {

    public String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader("Authorization");
        if(bearerToken!=null && bearerToken.startsWith("Bearer ")){
            return bearerToken.substring(7);
        }
        return null;
    }

    public String parseJWT(String accessToken){
        try{
            var decodedJWT = SignedJWT.parse(accessToken);
            return decodedJWT.getPayload().toString();
        } catch (ParseException e){
            throw new InvalidJwtTokenException();
        }
    }

    public UserData parsePayLoad(String payload){
        Gson gson = new Gson();
        UserData userData;

        try{
        userData = gson.fromJson(
                payload,
                UserData.class
        );} catch (RuntimeException exception){
            throw new InvalidJwtTokenException();
        }

        if (StringUtils.isEmpty(userData.getUsername())){
            throw new InvalidJwtTokenException();
        }

        return userData;
    }
}

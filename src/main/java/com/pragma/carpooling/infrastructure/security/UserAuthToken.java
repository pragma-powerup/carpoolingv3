package com.pragma.carpooling.infrastructure.security;


import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class UserAuthToken extends AbstractAuthenticationToken {

    private UserAuthPrincipal userAuthPrincipal;

    public UserAuthToken(UserAuthPrincipal userAuthPrincipal) {
        super(userAuthPrincipal.getAuthorities());
        super.setAuthenticated(true);
        this.userAuthPrincipal = userAuthPrincipal;
    }
    public UserAuthToken(String authToken, Collection<? extends GrantedAuthority> authorities){
        super(authorities);
        this.setAuthenticated(true);
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return userAuthPrincipal;
    }
}

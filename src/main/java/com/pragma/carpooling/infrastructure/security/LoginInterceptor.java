package com.pragma.carpooling.infrastructure.security;


import com.google.gson.stream.MalformedJsonException;
import com.pragma.carpooling.infrastructure.exception.MissingAuthorizationHeaderException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public class LoginInterceptor implements HandlerInterceptor {

    private final JwtUtils jwtUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String token = jwtUtils.resolveToken(request);
        if(StringUtils.isBlank(token)) {
            throw new MissingAuthorizationHeaderException();
        }
        String payload = jwtUtils.parseJWT(token);
        UserData userData = jwtUtils.parsePayLoad(payload);
        setAuthentication(userData);
        return true;
    }

    private void setAuthentication(UserData userData){
        Authentication newAuthentication = new UserAuthToken(new UserAuthPrincipal(userData));
        SecurityContextHolder.getContext().setAuthentication(newAuthentication);
    }
}

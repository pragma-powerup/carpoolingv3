package com.pragma.carpooling.infrastructure.exceptionHandler;

import com.pragma.carpooling.infrastructure.exception.BarriosRepetidosException;
import com.pragma.carpooling.infrastructure.exception.CamposInvalidosException;
import com.pragma.carpooling.infrastructure.exception.CognitoException;
import com.pragma.carpooling.infrastructure.exception.CuposInvalidosException;
import com.pragma.carpooling.infrastructure.exception.HorarioNullException;
import com.pragma.carpooling.infrastructure.exception.InvalidEmailException;
import com.pragma.carpooling.infrastructure.exception.InvalidJwtTokenException;
import com.pragma.carpooling.infrastructure.exception.InvalidPasswordException;
import com.pragma.carpooling.infrastructure.exception.MissingAuthorizationHeaderException;
import com.pragma.carpooling.infrastructure.exception.NoDataFoundException;
import com.pragma.carpooling.infrastructure.exception.NoHaySuficientesBarriosException;
import com.pragma.carpooling.infrastructure.exception.UserAlreadyExistsException;
import com.pragma.carpooling.infrastructure.exception.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvise {

    private static final String MESSAGE = "Message";

    @ExceptionHandler(UserAlreadyExistsException.class)
    public ResponseEntity<Map<String, String>> handleUserAlreadyExistsException(
            UserAlreadyExistsException userAlreadyExistsException){
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_ALREADY_EXISTS.getMessage()));
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Map<String, String>> handleUserNotFoundException(
            UserNotFoundException userNotFoundException){
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_FOUND.getMessage()));
    }

    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Map<String, String>> handleNoDataFoundException(
            NoDataFoundException noDataFoundException){
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.NO_DATA_FOUND.getMessage()));
    }

    @ExceptionHandler(BarriosRepetidosException.class)
    public ResponseEntity<Map<String, String>> handleBarriosRepetidosException(
            BarriosRepetidosException barriosRepetidosExceptionException){
        return ResponseEntity.status(HttpStatus.CONFLICT)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.BARRIOS_REPETIDOS.getMessage()));
    }

    @ExceptionHandler(NoHaySuficientesBarriosException.class)
    public ResponseEntity<Map<String, String>> handleNoHaySuficientesBarriosException(
            NoHaySuficientesBarriosException noHaySuficientesBarriosException){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.INSUFICIENTES_BARRIOS.getMessage()));
    }

    @ExceptionHandler(CuposInvalidosException.class)
    public ResponseEntity<Map<String, String>> handleCuposInvalidosException(
            CuposInvalidosException CuposInvalidosException){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.CUPOS_INVALIDOS.getMessage()));
    }

    @ExceptionHandler(HorarioNullException.class)
    public ResponseEntity<Map<String, String>> handleHorarioNullException(
            HorarioNullException horarioNullException){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.HORARIO_NULL.getMessage()));
    }

    @ExceptionHandler(InvalidPasswordException.class)
    public ResponseEntity<Map<String, String>> handleInvalidPasswordException(
            InvalidPasswordException invalidPasswordException){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.PASSWORD_INVALIDA.getMessage()));
    }

    @ExceptionHandler(InvalidEmailException.class)
    public ResponseEntity<Map<String, String>> handleInvalidEmailException(
            InvalidEmailException invalidEmailException){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.EMAIL_INVALIDO.getMessage()));
    }

    @ExceptionHandler(CamposInvalidosException.class)
    public ResponseEntity<Map<String, String>> handleCamposInvalidosException(
            CamposInvalidosException camposInvalidosException){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(Collections.singletonMap(MESSAGE, camposInvalidosException.getMessage()));
    }

    @ExceptionHandler(MissingAuthorizationHeaderException.class)
    public ResponseEntity<Map<String, String>> handleMissingAuthorizationHeaderException(
            MissingAuthorizationHeaderException missingAuthorizationHeaderException){
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.MISSING_AUTHORIZATION.getMessage()));
    }

    @ExceptionHandler(CognitoException.class)
    public ResponseEntity<Map<String, String>> handleCognitoException(
            CognitoException cognitoException){
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.COGNITO_EXCEPTION.getMessage()));
    }

    @ExceptionHandler(InvalidJwtTokenException.class)
    public ResponseEntity<Map<String, String>> handleInvalidJwtTokenException(
            InvalidJwtTokenException invalidJwtTokenException){
        return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED)
                .body(Collections.singletonMap(MESSAGE, ExceptionResponse.JWT_TOKEN_EXCEPTION.getMessage()));
    }


}

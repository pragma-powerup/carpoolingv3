package com.pragma.carpooling.infrastructure.exceptionHandler;

public enum ExceptionResponse {

    USER_NOT_FOUND("No existe usuario con este email"),
    NO_DATA_FOUND("No se encontraron datos"),
    USER_ALREADY_EXISTS("Ya existe un usuario con este email"),
    BARRIOS_REPETIDOS("No pueden haber barrios con el mismo nombre repetidos en una ruta"),
    INSUFICIENTES_BARRIOS("No hay suficientes barrios para registrar una ruta"),
    CUPOS_INVALIDOS("Cantidad de cupos inválidos, debe estar entre 4 y 1 (incluyéndolos)"),
    PASSWORD_INVALIDA("La contraseña debe tener entre 8 y 15 caracteres.También debe contener por lo menos: 1 letra en mayúscula, 1 letra en minúscula 1  número y un caracter de los siguientes: *_-"),
    EMAIL_INVALIDO("Email no cumple con las características"),
    MISSING_AUTHORIZATION("No tiene autorización para realizar esta acción"),
    COGNITO_EXCEPTION("Error en la operación con Cognito"),
    JWT_TOKEN_EXCEPTION("Token JWT inválido"),
    HORARIO_NULL("No se puede registrar un viaje con horario null");


    private String message;

    ExceptionResponse(String message){
        this.message = message;
    }

    public String getMessage(){
        return this.message;
    }
}

package com.pragma.carpooling.infrastructure.input.rest;

import com.pragma.carpooling.application.dto.request.UsuarioLogInRequestDto;
import com.pragma.carpooling.application.dto.request.UsuarioRequest;
import com.pragma.carpooling.application.dto.response.MensajeResponseDto;
import com.pragma.carpooling.application.dto.response.UsuarioLoginResponseDto;
import com.pragma.carpooling.application.handler.IUsuarioHandler;
import com.pragma.carpooling.domain.model.MensajeRespuesta;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/usuario/")
@RequiredArgsConstructor
public class UsuarioRestController {

    private final IUsuarioHandler usuarioHandler;

    @PostMapping("signup")
    public ResponseEntity<MensajeResponseDto> signup(@RequestBody UsuarioRequest usuarioRequest){
        return new ResponseEntity<>(usuarioHandler.registrarUsuario(usuarioRequest), HttpStatus.CREATED);
    }

    @PostMapping("login")
    public ResponseEntity<UsuarioLoginResponseDto> login(@RequestBody UsuarioLogInRequestDto usuarioLogInRequestDto){
        return new ResponseEntity<>(usuarioHandler.iniciarSesion(usuarioLogInRequestDto), HttpStatus.CREATED);
    }
}

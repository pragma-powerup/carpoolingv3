package com.pragma.carpooling.infrastructure.input.rest;

import com.pragma.carpooling.application.dto.request.RutaCompletaRequest;
import com.pragma.carpooling.application.dto.response.MensajeResponseDto;
import com.pragma.carpooling.application.handler.IRutaHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/ruta/")
@RequiredArgsConstructor
public class RutaRestController {

    private final IRutaHandler rutaHandler;

    @PostMapping("guardarRutaCompleta")
    public ResponseEntity<MensajeResponseDto> guardarRutaCompleta(@RequestBody RutaCompletaRequest rutaCompletaRequest){
        return new ResponseEntity<>(rutaHandler.guardarRutaCompleta(rutaCompletaRequest),HttpStatus.CREATED);
    }
}

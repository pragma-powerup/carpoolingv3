package com.pragma.carpooling.infrastructure.input.rest;

import com.pragma.carpooling.application.handler.IBarrioHandler;
import com.pragma.carpooling.domain.model.Barrio;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/barrio")
@RequiredArgsConstructor
public class BarrioRestController {

    private final IBarrioHandler barrioHandler;

    @GetMapping("/listarBarrios")
    public ResponseEntity<List<Barrio>> listarBarrios(){
        return ResponseEntity.ok(barrioHandler.listarBarios());
    }
}

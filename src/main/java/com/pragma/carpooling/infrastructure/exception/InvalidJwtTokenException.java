package com.pragma.carpooling.infrastructure.exception;


public class InvalidJwtTokenException extends RuntimeException{
    public InvalidJwtTokenException() {
        super();
    }
}

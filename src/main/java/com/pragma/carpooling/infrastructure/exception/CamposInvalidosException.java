package com.pragma.carpooling.infrastructure.exception;

public class CamposInvalidosException extends RuntimeException{
    public CamposInvalidosException(String message) {
        super(message);
    }
}

package com.pragma.carpooling.infrastructure.exception;

public class MissingAuthorizationHeaderException extends RuntimeException{
    public MissingAuthorizationHeaderException() {
        super();
    }
}

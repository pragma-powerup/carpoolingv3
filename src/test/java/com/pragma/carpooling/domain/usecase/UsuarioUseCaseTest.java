package com.pragma.carpooling.domain.usecase;

import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.spi.ICognitoPersistencePort;
import com.pragma.carpooling.domain.spi.IUsuarioPersistencePort;
import com.pragma.carpooling.factory.UsuarioDataTestFactory;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
class UsuarioUseCaseTest {

    @InjectMocks
    UsuarioUseCase usuarioUseCase;

    @Mock
    IUsuarioPersistencePort usuarioPersistencePort;

    @Mock
    ICognitoPersistencePort cognitoPersistencePort;


    @Test
    void deberiaGuardarUsuario() {
        //Given
        Usuario usuario = UsuarioDataTestFactory.getUsuario();

        //When
        when(cognitoPersistencePort.signupUser(any())).thenReturn(usuario.getUsername());
        when(usuarioPersistencePort.guardarUsuario(any())).thenReturn(usuario);
        usuarioUseCase.guardarUsuario(usuario);

        //Then
        verify(usuarioPersistencePort).guardarUsuario(usuario);
    }

    @Test
    void deberiaObetnerUsuarioPorSuEmail(){
        //When
        usuarioUseCase.buscarUsuarioPorEmail(anyString());

        //Then
        verify(usuarioPersistencePort).obtenerUsuarioPorEmail(anyString());
    }

    @Test
    void deberiaIniciarSesionDadoUnEmailYPassword(){

        Usuario usuario = UsuarioDataTestFactory.getUsuario();

        when(usuarioPersistencePort.obtenerUsuarioPorPasswordeEmail(usuario)).thenReturn(usuario);
        usuarioUseCase.iniciarSesion(usuario);

        verify(cognitoPersistencePort).loginUser(usuario);
    }


}
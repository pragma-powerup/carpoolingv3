package com.pragma.carpooling.domain.usecase;

import com.pragma.carpooling.domain.api.IBarrioServicePort;
import com.pragma.carpooling.domain.api.IRutaBarrioServicePort;
import com.pragma.carpooling.domain.api.IUsuarioServicePort;
import com.pragma.carpooling.domain.api.IViajeServicePort;
import com.pragma.carpooling.domain.model.Barrio;
import com.pragma.carpooling.domain.model.Ruta;
import com.pragma.carpooling.domain.model.Usuario;
import com.pragma.carpooling.domain.model.Viaje;
import com.pragma.carpooling.domain.spi.IRutaPersistencePort;
import com.pragma.carpooling.domain.spi.IUsuarioPersistencePort;
import com.pragma.carpooling.factory.BarrioFactoryData;
import com.pragma.carpooling.factory.RutaDataTestFactory;
import com.pragma.carpooling.factory.UsuarioDataTestFactory;
import com.pragma.carpooling.factory.ViajeDataFactory;
import com.pragma.carpooling.infrastructure.exception.CuposInvalidosException;
import com.pragma.carpooling.infrastructure.out.jpa.entity.UsuarioEntity;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class RutaUseCaseTest {

    @InjectMocks
    RutaUseCase rutaUseCase;

    @Mock
    IRutaPersistencePort rutaPersistencePort;

    @Mock
    IUsuarioServicePort usuarioServicePort;

    @Mock
    IBarrioServicePort barrioServicePort;

    @Mock
    IViajeServicePort viajeServicePort;

    @Mock
    IRutaBarrioServicePort rutaBarrioServicePort;

    @Test
    void expectCuposInvalidosException_whenCuposInvalidos() {

        //Given
        Ruta ruta = RutaDataTestFactory.getRuta();
        Usuario usuario = UsuarioDataTestFactory.getUsuario();
        List<Barrio> barriosList = Arrays.asList(BarrioFactoryData.getBarrioUno(), BarrioFactoryData.getBarrioDos());
        List<Viaje> viajesList = Arrays.asList(ViajeDataFactory.getViajeUno(), ViajeDataFactory.getViajeDos());
        ruta.setCupos(0);

        //When
        Throwable throwable = catchThrowable(() -> rutaUseCase.guardarRuta(ruta, usuario, barriosList, viajesList));

        //Then
        assertThat(throwable).isExactlyInstanceOf(CuposInvalidosException.class);
    }

    @Test
    void mustGuardarRuta_whenGuardarRuta() {

        //Given
        Ruta ruta = RutaDataTestFactory.getRuta();
        Usuario usuario = UsuarioDataTestFactory.getUsuario();
        List<Barrio> barriosList = Arrays.asList(BarrioFactoryData.getBarrioUno(), BarrioFactoryData.getBarrioDos());
        List<Viaje> viajesList = Arrays.asList(ViajeDataFactory.getViajeUno(), ViajeDataFactory.getViajeDos());

        //When
        when(usuarioServicePort.buscarUsuarioPorEmail(anyString())).thenReturn(usuario);
        when(barrioServicePort.guardarListabarrios(anyList())).thenReturn(barriosList);
        when(rutaPersistencePort.guardarRuta(ruta)).thenReturn(ruta);
        doNothing().when(rutaUseCase).
        //when(rutaBarrioServicePort.guardarRutaBarrio(barriosList, anyLong()));
//        when(viajeServicePort.guardarListaViajes(viajesList, ruta.getIdRuta()));
        rutaUseCase.guardarRuta(ruta, usuario, barriosList, viajesList);

        //Then
        verify(rutaPersistencePort).guardarRuta(ruta);
    }
}
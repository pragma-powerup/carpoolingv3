package com.pragma.carpooling.domain.model;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
public class UsuarioTest {

    @Test
    void mustGetAndSetAndSetId() {
        Usuario usuario = new Usuario();
        usuario.setIdUsuario(1L);
        assertEquals(1L, usuario.getIdUsuario());
    }

    @Test
    void mustGetAndSetName() {
        Usuario usuario = new Usuario();
        usuario.setName("Jose");
        assertEquals("Jose", usuario.getName());
    }

    @Test
    void mustGetAndSetFamilyName() {
        Usuario usuario = new Usuario();
        usuario.setFamilyName("Arrautt");
        assertEquals("Arrautt", usuario.getFamilyName());
    }

    @Test
    void mustGetAndSetEmail() {
        Usuario usuario = new Usuario();
        usuario.setEmail("jose@email.com");
        assertEquals("jose@email.com", usuario.getEmail());
    }

}

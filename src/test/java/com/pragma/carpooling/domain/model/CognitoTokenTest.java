package com.pragma.carpooling.domain.model;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class CognitoTokenTest {

    @Test
    void mustSetAndGetAccessToken(){
        CognitoToken cognitoToken = new CognitoToken();
        cognitoToken.setAccessToken("accessToken");
        assertEquals("accessToken", cognitoToken.getAccessToken());
    }

    @Test
    void mustSetAndGetIdToken(){
        CognitoToken cognitoToken = new CognitoToken();
        cognitoToken.setIdToken("idToken");
        assertEquals("idToken", cognitoToken.getIdToken());
    }

    @Test
    void mustSetAndGetRefreshToken() {
        CognitoToken cognitoToken = new CognitoToken();
        cognitoToken.setRefreshToken("refreshToken");
        assertEquals("refreshToken", cognitoToken.getRefreshToken());
    }

}

package com.pragma.carpooling.domain.model;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class RutaTest {

    @Test
    void setId(){
        Ruta ruta = new Ruta();
        ruta.setIdRuta(1L);
        assertEquals(1L, ruta.getIdRuta());
    }

}

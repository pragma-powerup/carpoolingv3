package com.pragma.carpooling.factory;

import com.pragma.carpooling.domain.model.Usuario;

public class UsuarioDataTestFactory {


    public static Usuario getUsuario(){
        Usuario usuario = new Usuario();
        usuario.setIdUsuario(1l);
        usuario.setName("Juan");
        usuario.setFamilyName("Castro");
        usuario.setEmail("juan.castro@1.com");
        usuario.setPassword("HolaMundo1234*");
        usuario.setAddress("Calle falsa #1234");
        usuario.setPhoneNumber("+573007698329");
        return usuario;
    }
}
